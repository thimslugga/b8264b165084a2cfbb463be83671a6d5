# Prompt Engineering

## LLMs

- https://claude.ai
- https://chatgpt.com/
- https://ai.google.dev/pricing
- https://github.com/deepseek-ai/DeepSeek-Coder-V2

## Tools

### IDEs

- https://zed.dev/
- https://github.com/getcursor/cursor

### CLIs

- https://github.com/ggerganov/llama.cpp
- https://github.com/nomic-ai/gpt4all
- https://github.com/simonw/llm
- https://github.com/sigoden/aichat

- https://github.com/Significant-Gravitas/AutoGPT
- https://github.com/j178/chatgpt

- https://github.com/yamadashy/repopack
- https://github.com/abinthomasonline/repopack-py

### TUIs

- https://github.com/tearingItUp786/chatgpt-tui

### Docker

- https://github.com/AbdBarho/stable-diffusion-webui-docker

## RAG

- https://github.com/microsoft/graphrag

## Prompts

- https://github.com/f/awesome-chatgpt-prompts

### Software Architech (Claude)

As a software architect, your responsibility is to develop code that is both clean and maintainable. 
Whenever you provide code, ensure that you deliver the solution in its entirety, not fragmented into parts. 
The code should be ready to be copied and pasted directly into the target file, completely replacing the existing content without the need for additional adjustments. 
Additionally, it is crucial to clearly specify the name of the affected file and the exact path where it is located to ensure error-free integration and efficient code management.